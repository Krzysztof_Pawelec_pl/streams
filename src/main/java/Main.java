import com.paw.exercise.streamlambda.StreamLambda;

public class Main {

    public static void main(String[] args) {
        String path = "src/main/resources/makbet.txt";

        StreamLambda streamLambda = new StreamLambda();

        System.out.println("Liczba linii: " + streamLambda.countLines(path));
        System.out.println("Liczba niepustych linii: " + streamLambda.withoutEmptyLines(path));
        System.out.println("Liczba wyrazów: " + streamLambda.countWords(path));// lista słów
        System.out.println("Extra lista wyrazów: " + streamLambda.extraWordsList(path));
        System.out.println("Liczba występujących słów : " + streamLambda.countExistsWords(path));
        System.out.println("Liczba unikalnych słów : " + streamLambda.countUnicWords(path));
        System.out.println("Lista występujących słów : " + streamLambda.existsWordsList(path)); // nie wyświetlam przez for each, ponieważ strasznie utrudnia to pracę.
        System.out.println("Lista unikalnych słów : " + streamLambda.unicWordsList(path));
        System.out.println("Cały dokument ze słowami z literą 's': " + streamLambda.changeWordsWithS(path));
        System.out.println("Cały dokument bez słów zaczynających się dużą literą: " + streamLambda.removeWordsWithHugeChar(path));
        System.out.println("Liczby zamiast słów: " + streamLambda.countCharsInWords(path));
        System.out.println(streamLambda.changeWords(path));


        for (String line : streamLambda.withoutEmptyLines(path)) { // wczytanie i wyświetlenie liczby linii - jako pierwszy element listy
            System.out.println(line);
        }
    }
}
