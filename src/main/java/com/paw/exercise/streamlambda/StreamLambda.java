package com.paw.exercise.streamlambda;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class StreamLambda {

    public Stream<String> inputStream(String fileName) {
        Stream<String> lines = null;
        if (fileName == null || fileName.isEmpty()) {
            return Stream.empty();
        }
        try {
            lines = Files.lines(Paths.get(fileName));
        } catch (IOException e) {
            System.out.println("Błąd odczytu pliku. " + e);
        }
        return lines;
    }


    public float countLines(String fileName) {
        return inputStream(fileName).count();
    }

    public List<String> withoutEmptyLines(String fileName) {
        List<String> makbet = inputStream(fileName)
                .filter(line -> !line.isEmpty())
                .collect(toList());
        makbet.add(0, "Liczba niepustych linii: " + makbet.size() + "\n");
        return makbet;
    }

    public float countWords(String fileName) {
        return inputStream(fileName)
                .filter(line -> !line.isEmpty())
                .map(line -> line.replaceAll("[?/.,—()!{}-]", ""))
                .map(line -> line.replaceAll("[0-9]", ""))
                .flatMap(line -> Stream.of(line.split("\\s")))
                .filter(line -> line.length() > 0)
                .count();
    }

    public List<String> extraWordsList(String fileName) {
        return inputStream(fileName)
                .filter(line -> !line.isEmpty())
                .map(line -> line.replaceAll("[?/.,—()!{}-]", ""))
                .map(line -> line.replaceAll("[0-9]", ""))
                .flatMap(line -> Stream.of(line.split("\\s")))
                .collect(toList());
    }

    public float countExistsWords(String fileName) {
        return inputStream(fileName)
                .filter(line -> !line.isEmpty())
                .map(line -> line.replaceAll("[?/.,—()!{}-]", ""))
                .map(line -> line.replaceAll("[0-9]", ""))
                .flatMap(line -> Stream.of(line.split("\\s")))
                .map(String::toLowerCase)
                .distinct()
                .count();
    }

    public float countUnicWords(String fileName) {
        Map<String, Integer> collect = inputStream(fileName)
                .filter(line -> !line.isEmpty())
                .map(line -> line.replaceAll("[?/.,—()!{}-]", ""))
                .map(line -> line.replaceAll("[0-9]", ""))
                .flatMap(line -> Stream.of(line.split("\\s")))
                .map(String::toLowerCase)
                .collect(Collectors.toMap(word -> word, word -> 1, Integer::sum));

        return collect.entrySet().stream()
                .filter(entry -> entry.getValue() == 1)
                .count();
    }

    public List<String> unicWordsList(String fileName) {
        Map<String, Integer> collect = inputStream(fileName)
                .filter(line -> !line.isEmpty())
                .map(line -> line.replaceAll("[?/.,—()!{}-]", ""))
                .map(line -> line.replaceAll("[0-9]", ""))
                .flatMap(line -> Stream.of(line.split("\\s")))
                .map(String::toLowerCase)
                .collect(Collectors.toMap(word -> word, word -> 1, Integer::sum));

        return collect.entrySet().stream()
                .filter(entry -> entry.getValue() == 1)
                .flatMap(entry -> Stream.of(entry.getKey()))
                .collect(toList());
    }

    public List<String> existsWordsList(String fileName) {
        return inputStream(fileName)
                .filter(line -> !line.isEmpty())
                .map(line -> line.replaceAll("[?/.,—()!{}-]", ""))
                .map(line -> line.replaceAll("[0-9]", ""))
                .flatMap(line -> Stream.of(line.split("\\s")))
                .map(String::toLowerCase)
                .distinct()
                .collect(toList());
    }

    public String changeWordsWithS(String fileName) {
        return inputStream(fileName)
                .map(line -> Pattern.compile("\\s").splitAsStream(line)
                        .map(word -> word.contains("s") || word.contains("S") ? word.toUpperCase() : word)
                        .collect(Collectors.joining(" ")))
                .collect(Collectors.joining("\n"));
    }

    public String removeWordsWithHugeChar(String fileName) {
        return inputStream(fileName)
                .map(line -> Pattern.compile("\\s").splitAsStream(line)
                        .filter(word -> word.isEmpty() || !Character.isUpperCase(word.charAt(0)))
                        .collect(Collectors.joining(" ")))
                .collect(Collectors.joining("\n"));
    }

    public String countCharsInWords(String fileName) {
        return inputStream(fileName)
                .map(line -> Pattern.compile("\\s").splitAsStream(line)
                        .map(word -> String.valueOf(word.length()))
                        .collect(Collectors.joining(" ")))
                .collect(Collectors.joining("\n"));
    }

    public String changeWords(String fileName) {
        return inputStream(fileName)
                .map(line -> Pattern.compile("\\s").splitAsStream(line)
                        .map(word -> word.contains("Król") ? "Cesarz" : word)
                        .collect(Collectors.joining(" ")))
                .collect(Collectors.joining("\n"));
    }


}
