package com.paw.exercise.testsstreamlambda;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.paw.exercise.streamlambda.StreamLambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;


public class TestStreamLambda {

    private StreamLambda streamLambda = new StreamLambda();

    private List<String> emptyList = new ArrayList<String>();

    private String path1 = "src/main/resources/makbetPartForTests.txt";
    private String path2 = "src/main/resources/makberPartGroundTrue.txt";
    private String path3 = "";


    @Test
    void test_countLines() {
        assertEquals(4, streamLambda.countLines(path1));
        assertEquals(0, streamLambda.countLines(path2));
        assertEquals(0, streamLambda.countLines(path3));
    }

    @Test
    void test_withoutEmptyLines() {
        List<String> list = new ArrayList<>();
        list.add("Liczba niepustych linii: 3\n");
        list.add("Król");
        list.add("król");
        list.add("Stefan 1");

        assertEquals(list, streamLambda.withoutEmptyLines(path1));
        assertEquals(Arrays.asList("Liczba niepustych linii: 0\n"), streamLambda.withoutEmptyLines(path2));
        assertEquals(Arrays.asList("Liczba niepustych linii: 0\n"), streamLambda.withoutEmptyLines(path3));
    }

    @Test
    void test_countWords() {
        assertEquals(3, streamLambda.countWords(path1));
        assertEquals(0, streamLambda.countWords(path2));
        assertEquals(0, streamLambda.countWords(path3));
    }

    @Test
    void test_extraWordsList() {
        List<String> list = new ArrayList<>();
        list.add("Król");
        list.add("król");
        list.add("Stefan");
        assertEquals(list, streamLambda.extraWordsList(path1));
        assertEquals(emptyList, streamLambda.extraWordsList(path2));
        assertEquals(emptyList, streamLambda.extraWordsList(path3));
    }

    @Test
    void test_countExistsWords() {
        assertEquals(2, streamLambda.countExistsWords(path1));
        assertEquals(0, streamLambda.countExistsWords(path2));
        assertEquals(0, streamLambda.countExistsWords(path3));
    }

    @Test
    void test_countUnicWords() {
        assertEquals(1, streamLambda.countUnicWords(path1));
        assertEquals(0, streamLambda.countUnicWords(path2));
        assertEquals(0, streamLambda.countUnicWords(path3));
    }

    @Test
    void test_existsWordsList() {
        List<String> list = new ArrayList<>();
        list.add("król");
        list.add("stefan");
        assertEquals(list, streamLambda.existsWordsList(path1));
        assertEquals(emptyList, streamLambda.existsWordsList(path2));
        assertEquals(emptyList, streamLambda.existsWordsList(path3));
    }


    @Test
    void test_unicWordsList() {
        List<String> list = new ArrayList<>();
        list.add("stefan");
        assertEquals(list, streamLambda.unicWordsList(path1));
        assertEquals(emptyList, streamLambda.unicWordsList(path2));
        assertEquals(emptyList, streamLambda.unicWordsList(path3));
    }

    @Test
    void test_changeWordsWithS() {
        assertEquals("Król\nkról\n\nSTEFAN 1", streamLambda.changeWordsWithS(path1));
        assertEquals("", streamLambda.changeWordsWithS(path2));
        assertEquals("", streamLambda.changeWordsWithS(path3));
    }

    @Test
    void test_removeWordsWithHugeChar() {
        assertEquals("\nkról\n\n1", streamLambda.removeWordsWithHugeChar(path1));
        assertEquals("", streamLambda.removeWordsWithHugeChar(path2));
        assertEquals("", streamLambda.removeWordsWithHugeChar(path3));
    }

    @Test
    void test_countCharsInWords() {
        assertEquals("4\n4\n\n6 1", streamLambda.countCharsInWords(path1));
        assertEquals("", streamLambda.countCharsInWords(path2));
        assertEquals("", streamLambda.countCharsInWords(path3));
    }

    @Test
    void test_changeWords() {
        assertEquals("Cesarz\nkról\n\nStefan 1", streamLambda.changeWords(path1));
        assertEquals("", streamLambda.changeWords(path2));
        assertEquals("", streamLambda.changeWords(path3));
    }
}
